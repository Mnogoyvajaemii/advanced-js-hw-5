"use strict";

const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";
const postsFeed = document.querySelector(".posts-feed");

class Request {
  get(url) {
    return fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(),
    }).then((responce) => responce.json());
  }
  delete(url) {
    return fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(),
    }).then((response) => console.log(response));
  }
}

class Card {
  constructor() {
    this.request = new Request();
  }

  render(postObj) {
    let { body, title, name, email, id } = postObj;
    const post = document.createElement("div");
    post.classList.add("post");
    post.setAttribute("id", id);

    const postImage = document.createElement("img");
    postImage.setAttribute("src", "./img/user.svg");
    postImage.classList.add("post__image");

    const postContent = document.createElement("div");
    postContent.classList.add("post__content");

    const postAuthor = document.createElement("div");
    postAuthor.classList.add("author-name");

    const authorName = document.createElement("p");
    authorName.classList.add("author-email");
    authorName.textContent = name;

    const authorEmail = document.createElement("p");
    authorEmail.classList.add("post__author");
    authorEmail.textContent = email;

    postAuthor.append(authorName, authorEmail);

    const postTitle = document.createElement("h2");
    postTitle.classList.add("post__title");
    postTitle.textContent = title;

    const postDescr = document.createElement("p");
    postDescr.classList.add("post__description");
    postDescr.textContent = body;

    const postBtn = document.createElement("button");
    postBtn.classList.add("post__button");
    postBtn.innerHTML = "&#10006;";

    postBtn.addEventListener("click", (event) => {
      event.preventDefault();
      let btnId = id;
      return this.request.delete(postsUrl + `/${btnId}`).then(() => {
        event.target.closest(".post").remove();
      });
    });

    postContent.append(postAuthor, postTitle, postDescr);
    post.append(postImage, postContent, postBtn);

    return post;
  }
}

class Post {
  constructor() {
    this.request = new Request();
  }

  getPosts() {
    return this.request.get(postsUrl).then((data) => {
      data.forEach((post) => {
        return this.request.get(usersUrl).then((data) => {
          data.forEach((user) => {
            let { id } = user;
            let { userId } = post;
            if (id === userId) {
              const userPost = Object.assign({}, user, post);
              const card = new Card();
              postsFeed.append(card.render(userPost));
            }
          });
        });
      });
    });
  }
}

const post = new Post();

post.getPosts();
